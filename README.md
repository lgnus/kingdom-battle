# Kingdom Battle

Battle Network inspired game developed using Godot 3.0.6 and recently ported
to 3.1.2

Passion project developed to learn the ropes of the Godot search engine. Many, 
many improvements and features still to implement.