extends Node

# Directions
const UP = Vector2(0, -1)
const DOWN = Vector2(0, 1)
const LEFT = Vector2(-1, 0)
const RIGHT = Vector2(1, 0)

# status effects
#enum STATUS {root, silence, poison, protect} # TODO add more

const STATUS = {
	"root": 0,
	"silence": 1, 
	"poison": 2,
	"protect": 3,
	"channeling": 4
}

#const STATUS = ["root","silence","poison","protect"]

# DEBUG
func get_status_name( id ):
	match id:
		0: return "root"
		1: return "silence"
		2: return "poison"
		3: return "protect"
		4: return "channeling"