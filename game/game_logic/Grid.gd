extends Node2D

# Grid size
export(Vector2) var grid_size = Vector2(6,3)
var tile_size = Vector2()

# Layout size variables
export(Vector2) var top_left_offset = Vector2(0, .1)
export(Vector2) var bottom_right_offset = Vector2(0, .1)

var top_left
var bottom_right

# Logical grid
var grid = {}
onready var GridPanel = preload("res://game/game_logic/Panel.tscn")

# Drawing variables
export(Color) var line_color = Color(255,255,255,1)
export(Color) var division_color = Color(190,0,0,1)
export(int) var line_width = 2


func _ready():
	# Obtain window size dependent variables
	top_left = top_left_offset * OS.window_size  
	bottom_right = OS.window_size - (bottom_right_offset * OS.window_size)
	
	# Set grid draw to proper location and size
	$color.margin_top = top_left.y
	$color.margin_left = top_left.x
	$color.margin_right = bottom_right.x
	$color.margin_bottom = bottom_right.y
	
	# Calculate the tile size and set self as game grid + global tile size
	tile_size = (bottom_right - top_left) / grid_size
	Global.tile_size = tile_size 
	Global.grid = self
	
	# Initialize logical grid
	for x in range(grid_size.x):
		for y in range(grid_size.y):
			# create new panel instance and add to grid
			var new_panel = GridPanel.instance()
			new_panel.init(Vector2(x,y), grid_size, tile_size, grid_to_world(Vector2(x,y)))
			grid[Vector2(x,y)] = new_panel
			add_child(new_panel)


func _process(delta):
	# DEBUG: REMOVE
	if Input.is_action_just_pressed("debug"):
		var grid_mouse_pos = world_to_grid(get_viewport().get_mouse_position()) 
		if not grid_mouse_pos == null:
			grid[grid_mouse_pos].toggle_enabled()


func move_requested(entity, grid_pos):
	# if the grid_pos is no within bounds, do nothing
	if not within_bounds(grid_pos): return false
	
	# if the grid_pos is empty and has the same affiliation, move it
	if grid[grid_pos].is_free(entity.floats) and grid[grid_pos].is_left == entity.is_left:
		# clear previous panel, set entity to new one and update entity grid pos and z-index
		grid[entity.grid_pos].clear()
		grid[grid_pos].set_entity(entity)
		entity.grid_pos = grid_pos
		entity.z_index = grid_pos.y
		
		# animate position
		$tween.interpolate_property(entity, "position", entity.position, grid_to_world(grid_pos), .1, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$tween.start()

func within_bounds(grid_pos):
	""" Returns true if the postion is within bounds """
	if grid_pos.x >= grid_size.x or grid_pos.x < 0 or grid_pos.y >= grid_size.y or grid_pos.y < 0:
		return false
	return true

func grid_to_world(grid_pos):
    """ Returns the world coordinates given the grid ones """
    return top_left + grid_pos * tile_size + (tile_size / 2)

func world_to_grid(world_pos):
	""" Returns the grid coordinates given the world ones """ 
	# check if it even is inside a grid position
	if world_pos.x < top_left.x or world_pos.x > bottom_right.x or world_pos.y < top_left.y or world_pos.y > bottom_right.y:
		return null
	else:
		var zero_world_pos = world_pos - top_left
		return Vector2(int(zero_world_pos.x / tile_size.x), int(zero_world_pos.y / tile_size.y))

func _draw():
    """ Draw a grid on the screen with defined criteria """
    for x in range(grid_size.x + 1):

        for y in range(grid_size.y + 1):
            draw_line(
                Vector2(top_left.x, top_left.y + y * tile_size.y), \
                Vector2(bottom_right.x, top_left.y + y * tile_size.y), \
			    line_color, line_width)

        if x != grid_size.x / 2: # Division line
            draw_line(
                Vector2(top_left.x + x * tile_size.x, top_left.y), \
                Vector2(top_left.x + x * tile_size.x, bottom_right.y), \
			    line_color, line_width)
        else:
            draw_line(
                Vector2(top_left.x + x * tile_size.x, top_left.y), \
                Vector2(top_left.x + x * tile_size.x, bottom_right.y), \
			    division_color, line_width)