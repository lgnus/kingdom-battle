extends Node2D

export(Texture) onready var broken
export(Texture) onready var grass
export(Texture) onready var normal

# reference to any entity on this panel
var entity = null

# if the panel is currently enabled (might be blocked, etc)
var enabled = true

# Affiliation
var is_left = false

func init( grid_pos , grid_size, tile_size, world_pos ):
	# set affiliation
	is_left = false if grid_pos.x >= grid_size.x/2 else true
	
	# Set drawing position
	$rect.rect_size = tile_size - Vector2(8,8)
	position = world_pos - (tile_size - Vector2(8,8))/2.0
	
	# if not enabled, change color
	if not enabled:
		$rect.texture = broken
	
	
func is_free(floats):
	""" Returns true if the panel is empty and available """
	if floats:
		return entity == null
	else:
		return true if enabled and entity == null else false


func clear():
	""" Clears any entities from this panel """
	entity = null

func apply_damage():
	""" Applies damage to the entity in this panel """
	entity.apply_damage()

func set_entity( new_entity ):
	""" Assigns a new entity to this panel """
	entity = new_entity 

func set_enabled(boolean):
	enabled = boolean
	
	if enabled:
		$rect.texture = normal
	else:
		$rect.texture = broken
		$special_duration.start()

func toggle_enabled():
	""" Switches enabled on / off """
	enabled = not enabled
	
	if enabled:
		$rect.texture = normal
	else:
		$rect.texture = broken

func _on_special_duration_timeout():
	$rect.texture = normal
	enabled = true
