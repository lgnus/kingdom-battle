extends Control

onready var player = $"../Player"

var status_progress = {}

func _ready():
	# Fetch all the skill icons
	$skills/skill_a.texture_under = Skill.get_icon(player.skill_a)
	$skills/skill_b.texture_under = Skill.get_icon(player.skill_b)
	$skills/skill_x.texture_under = Skill.get_icon(player.skill_x)
	$skills/skill_y.texture_under = Skill.get_icon(player.skill_y)
	
	# Fetch all the status effect icons
	for status in Status.STATUS:
		if status != Status.get_status_name(Status.STATUS.none):
			var aux = TextureProgress.new()
			aux.texture_progress = Status.load_tex
			aux.texture_under = Status.get_icon(Status.get_id(status)) # DEBUG
			aux.rect_scale = Vector2(0.03, 0.03)
			aux.fill_mode = TextureProgress.FILL_COUNTER_CLOCKWISE
			aux.visible = false
			status_progress[status] = aux
			$stat/status.add_child(aux)

func _process(delta):
	if get_parent().has_node("Player"): 
		_handle_skills()
		_handle_status()
		$stat/hp.value = player.hp
		$stat/mp.value = player.mp


func _handle_skills():
	# Update all the skills visual feedback
	$skills/skill_a.value = player.cooldowns[player.skill_a].time_left / player.cooldowns[player.skill_a].wait_time
	$skills/skill_b.value = player.cooldowns[player.skill_b].time_left / player.cooldowns[player.skill_b].wait_time
	$skills/skill_x.value = player.cooldowns[player.skill_x].time_left / player.cooldowns[player.skill_x].wait_time
	$skills/skill_y.value = player.cooldowns[player.skill_y].time_left / player.cooldowns[player.skill_y].wait_time

func _handle_status():
	var pos = 0
	for status in player.status_effects:
		if not player.status_effects[status].is_stopped():
			status_progress[status].visible = true
			status_progress[status].value = 100*(player.status_effects[status].time_left / player.status_effects[status].wait_time)
			status_progress[status].rect_position = Vector2(pos * 60 + 10, status_progress[status].rect_position.y)
			pos += 1
		else:
			status_progress[status].visible = false