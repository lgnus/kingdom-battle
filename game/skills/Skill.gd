extends Area2D

# Numerical vars
var speed_y = -11
var row = 0

export(float) var damage = 15
export(float) var travel_speed = 600

# skill flags
export(bool) var channeling = false # while it lasts, stay rooted
export(bool) var pierce = false # doesn't free itself after hitting an enemy
export(bool) var self_cast = false # skill targets its parent instead
export(bool) var melee = false # skill ends after animation and is always pierce, channeling as well??
export(bool) var beam = false
export(bool) var throwable = false

# Status vars
const StatusType = preload("res://game/autoload/status.gd").STATUS
export(StatusType) onready var self_status 
export(float) var self_status_duration = 0
export(StatusType) onready var status 
export(float) var status_duration = 0

# Others
var parent = null
var direction = Vector2()
var hitting = []

func _ready():
	# Change collision shapes to be tile-sized dependent
	if not melee:
		$collision_shape.shape.extents = Global.tile_size/2 * 0.6

func _physics_process(delta):
	
	# For throwable objects, add y speed and apply gravity
	if throwable:
		speed_y += gravity * delta
		position.y += speed_y
		
		if position.y > Global.grid_to_world(Vector2(0,row)).y:
			Global.modify_panel(Global.world_to_grid(position), false)
			queue_free()
	
	# if not melee or self_cast, move the particle
	if not melee and not self_cast and not beam:
		position += direction * delta * travel_speed
	
	# for beams, make the collision shape move instead of the skill itself
	if beam:
		
		if not weakref(parent).get_ref():
			queue_free()
		
		$collision_shape.shape.extents += Vector2(travel_speed, 0) #* direction
		$collision_shape.position += Vector2(travel_speed, 0) #* direction
		
		# apply damage tick
		for body in hitting:
			body.apply_damage( damage )


func init( parent ):
	# creates parent reference and set proper z_index
	self.parent = parent
	self.z_index = int(parent.grid_pos.y+1)
	self.row = parent.grid_pos.y
	
	# Change direction and collision properties according to parent affiliation
	if parent.is_left:
		direction = Direction.RIGHT
		collision_layer = Global.COLLISION_TYPE.left_areas
		collision_mask = Global.COLLISION_TYPE.right_bodies
	else:
		direction = Direction.LEFT
		collision_layer = Global.COLLISION_TYPE.right_areas
		collision_mask = Global.COLLISION_TYPE.left_bodies
		
		if not self_cast:
			rotation_degrees = 180
	
	# Apply self status if not None
	if self_status != Status.STATUS.none:
		parent.apply_status(self_status, self_status_duration)
	
	# if self cast apply damage to self
	if self_cast:
		position = parent.skill_origin * Vector2(0,1) # take only the y coord
		parent.apply_damage( damage )
		parent.add_child(self)
		
		# start the timer that triggers queue free
		$lifetime.start()
		
	else:
		position = parent.position + parent.skill_origin
		parent.get_parent().add_child(self)
	
	# if melee, play attack animation and stop
	if melee:
		$anim.play("attack_r") if parent.is_left else $anim.play("attack_l")
	
	if channeling:
		parent.apply_status(Status.STATUS.channeling, $lifetime.wait_time)
		
	
	# Start the particles
	$particles_travel.set_emitting(true)

func _on_screen_exited():
	""" Delete itself if out of screen """
	if not melee:
		queue_free()


func _on_body_entered(body):
	# if throable, check if it is on the same grid row
	if throwable and body.grid_pos.y != row: return
	
	# if the skills have status effects, apply them
	if status != Status.STATUS.none:
		body.apply_status(status, status_duration)
	
	# if it's a beam, add body to hitting structure
	if beam:
		hitting.append(body)
	
	# apply damage
	body.apply_damage(damage)
	
	# if not pierce or melee, delete the skill once it collides with a body
	if not pierce and not melee:
		queue_free()

func _on_animation_finished(anim_name):
	# clear the skill once the skill animation is finished
	queue_free()


func _on_lifetime_timeout():
	# clear the skill once its lifetime runs out
	queue_free()

func _on_body_exited(body):
	# for beams, if collision leaves the body, remove it from the hitting structure
	if beam:
		hitting.erase(body)
