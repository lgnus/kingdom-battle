extends "res://game/entities/Entity.gd"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	handle_input()

func handle_input():
	# reset direction
	direction = Vector2()
	
	# handle inputs
	if Input.is_action_just_pressed("skill_a"):
		use_skill(skill_a)
	elif Input.is_action_just_pressed("skill_b"):
		use_skill(skill_b)
	elif Input.is_action_just_pressed("skill_x"):
		use_skill(skill_x)
	elif Input.is_action_just_pressed("skill_y"):
		use_skill(skill_y)
	if Input.is_action_just_pressed("ui_up"):
		direction = Direction.UP
	elif Input.is_action_just_pressed("ui_down"):
		direction = Direction.DOWN
	elif Input.is_action_just_pressed("ui_right"):
		direction = Direction.RIGHT
	elif Input.is_action_just_pressed("ui_left"):
		direction = Direction.LEFT
		
	# if there's a change in direction, request move
	if direction != Vector2():
		request_move(direction)

func _on_animation_finished(anim_name):
	# Clear this entity once death animation finishes playing
	if anim_name == "dead":
		queue_free()
	