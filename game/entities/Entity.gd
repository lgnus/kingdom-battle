extends KinematicBody2D

# Affiliation
export(bool) var is_left = false

# Position of this entity on the grid
export(Vector2) var grid_pos = Vector2()
var direction = Vector2()

# Skills related
const SkillNames = preload("res://game/autoload/skills.gd").names
export(SkillNames) var skill_a
export(SkillNames) var skill_b
export(SkillNames) var skill_x
export(SkillNames) var skill_y

var cooldowns = {}

onready var skill_origin = $skill_origin.position

# Dict to hold the status effects and their timers <>
var status_effects = {}

# Entity stats
export(int) var max_hp = 100
export(int) var max_mp = 100
export(float) var mp_regen = 1.5
var hp = 100
var mp = 100

# Entity properties
export(bool) var floats = false

# Signal that requests movement of entities to the grid
signal move_requested(entity, new_position)
var tween = Tween.new()

# auxiliary variables
var target_hp
var target_mp

func _ready():
	# max value > max hp
	$stats/hp.max_value = max_hp
	hp = max_hp
	mp = max_mp
	target_hp = hp
	target_mp = mp
	
	# set collision shape to a factor of the tile size
	$collision_shape.shape.extents = Global.tile_size/2 * Vector2(0.5, 0.8)
	
	# Handle skill cooldown timerse
	_set_timers([skill_a, skill_b, skill_x, skill_y])
	
	# Handle status duration timers
	for status in Status.STATUS:
		if status != Status.get_status_name(Status.STATUS.none):
			status_effects[status] = Timer.new()
			status_effects[status].set_autostart(false)
			status_effects[status].set_one_shot(true)
			# add to node
			add_child(status_effects[status])
	
	# property tweener
	self.add_child(tween)
	
	# Connect to grid and set start position
	connect("move_requested", $"../Grid", "move_requested")
	emit_signal("move_requested", self, grid_pos)
	
	# Add groups
	if is_left:
		collision_layer = Global.COLLISION_TYPE.left_bodies
		add_to_group('left')
	else:
		collision_layer = Global.COLLISION_TYPE.right_bodies
		add_to_group('right')

func _physics_process(delta):
	# update status bars
	$stats/hp.value = hp
	$stats/mp.value = mp
	
	# If hp reaches 0, play death animation
	if int(hp) == 0 and not $anim.current_animation == "dead" :
		$anim.stop() # stop any ongoing animation
		$anim.play("dead")
		apply_status(Status.STATUS.silence, 10)
		apply_status(Status.STATUS.root, 10)

func request_move( direction ):
	# Check for root and channeling, else, emit move request
	if has_status([Status.STATUS.root, Status.STATUS.channeling]):
		 return 
	emit_signal("move_requested", self, grid_pos + direction)

func use_skill( skill_id ):
	# if entity is silenced or currently channeling do nothing
	if has_status([Status.STATUS.silence, Status.STATUS.channeling]): return
	
	# if skill is on cooldown, do nothing
	if not cooldowns[skill_id].is_stopped(): return 
	
	# if not enough mana to cast, do nothing
	if target_mp - Skill.get_mana_cost(skill_id) < 0: return
	
	# update target mp
	target_mp -= Skill.get_mana_cost(skill_id)
	
	# smooth out mp loss over time
	tween.interpolate_property(self, "mp", mp, target_mp, .5, \
	Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	
	# Start skill cooldown and initiate skill
	cooldowns[skill_id].start()
	Skill.get_skill(skill_id).init(self)

func apply_damage( value ):
	""" Applies damage to this entity """
	
	# Check for status such as protect and check if not an heal
	if has_status([Status.STATUS.protect]) and value >= 0: return
	
	# else, do the apply damage routine
	$anim.play("hit") # run hit animation
	
	# change the target_hp and clamp it between 0 and max_hp
	target_hp = clamp(target_hp - value, 0, max_hp)
	
	# Smooth out hp loss over time
	tween.interpolate_property(self, "hp", hp, target_hp, .5, \
	Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()


func apply_status( status, duration ):
	# don't apply status if character was protected
	if has_status([Status.STATUS.protect]): return
	
	# reset timer of status and set new wait time (defaults to 1)
	var app_status = status_effects[Status.get_status_name(status)]
	app_status.stop() # reset timer if already has status
	app_status.wait_time = duration
	app_status.start()

func has_status( status_list ):
	""" return true if the entity has any of the provided status list """
	for status in status_list:
		if not status_effects[Status.get_status_name(status)].is_stopped():
			return true
	return false

func _set_timers( skill_list ):
	# timer set ups.
	for skill in skill_list:
		cooldowns[skill] = Timer.new()
		cooldowns[skill].set_autostart(false)
		cooldowns[skill].set_one_shot(true)
		cooldowns[skill].wait_time = Skill.get_cooldown(skill)
		add_child(cooldowns[skill])

func _on_animation_finished(anim_name):
	# Clear this entity once death animation finishes playing
	if anim_name == "dead":
		queue_free()


func _on_regen_tick_timeout():
	# apply mana regen
	if target_mp + mp_regen > max_mp:
		target_mp = max_mp
	else: 
		target_mp += mp_regen
	
	# Interpolate
	tween.interpolate_property(self, "mp", mp, target_mp, .5, \
	Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
