extends "res://game/entities/Entity.gd"

func _ready():
	pass

func _ai_action_timeout():
	randomize()
	var rng_move = int(floor(rand_range(0,4)))

	match rng_move:
		0: direction = Direction.UP
		1: direction = Direction.DOWN
		2: direction = Direction.LEFT
		3: direction = Direction.RIGHT
	
	var rng_attack = int(floor(rand_range(0,3)))
	
	match rng_attack:
		0: use_skill(skill_a)
		1: use_skill(skill_b)
		2: use_skill(skill_x)
		3: use_skill(skill_y)
	
	if randf() > 0.6:
		direction = Vector2()
	
	if direction != Vector2():
		request_move(direction)
	
	if randf() > .3:
		return


