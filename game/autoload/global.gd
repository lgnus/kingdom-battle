extends Node

onready var grid
var tile_size = Vector2()

enum COLLISION_TYPE { 
	left_bodies = 1, 
	right_bodies = 2, 
	left_areas = 4, 
	right_areas = 8
}

func modify_panel(grid_pos, boolean):
	grid.grid[grid_pos].set_enabled(boolean)

func grid_to_world(grid_pos):
	return grid.grid_to_world(grid_pos)

func world_to_grid(world_pos):
	return grid.world_to_grid(world_pos)