extends Node

func _ready():
	pass

# Names of all the skills, for export reasons
enum names {
	
	# Self Cast
	heal,
	shield,
	
	# Melee
	sword,
	
	# Ranged
	fireball,
	
	# Throwable
	grenade,
	
	# Channeling
	flamethrower
}

# List all the skills here, with following format:
# name -> scene | cooldown | icon | mana_cost 
onready var val = {
	names.heal: [preload("res://game/skills/Heal.tscn"), 10 , preload("res://assets/textures/user_interface/skills/heal-jade-1.png"), 10],
	names.shield: [preload("res://game/skills/Shield.tscn"), 8 , preload("res://assets/textures/user_interface/skills/protect-blue-1.png"), 10],
	names.fireball: [preload("res://game/skills/Fireball.tscn"), 0.5, preload("res://assets/textures/user_interface/skills/fireball-red-1.png"), 5],
	names.flamethrower: [preload("res://game/skills/Flamethrower.tscn"), 5 , preload("res://assets/textures/user_interface/skills/beam-orange-2.png"), 10],
	names.sword: [preload("res://game/skills/Sword.tscn"), 1, preload("res://assets/textures/user_interface/skills/enchant-sky-1.png"), 10],
	names.grenade: [preload("res://game/skills/Throwable.tscn"), 6, preload("res://assets/textures/user_interface/skills/fireball-red-1.png"), 15]
}

# Getters for all the attributes
func get_skill( skill_id ):
	return val[skill_id][0].instance()

func get_cooldown( skill_id ):
	return val[skill_id][1]

func get_mana_cost( skill_id ):
	return val[skill_id][3]

func get_icon(skill_id):
	return val[skill_id][2]
