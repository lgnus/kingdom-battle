extends Node

onready var load_tex = preload("res://assets/textures/user_interface/status/load.png")

enum STATUS{none=0, root=1, silence=2, poison=3, protect=4, channeling=5}

func get_id( name ):
	match name:
		"none": return STATUS.none
		"root": return STATUS.root
		"silence": return STATUS.silence
		"poison": return STATUS.poison
		"protect": return STATUS.protect
		"channeling": return STATUS.channeling
		_: return STATUS.none


func get_status_name( id ):
	match id:
		0: return "none"
		1: return "root"
		2: return "silence"
		3: return "poison"
		4: return "protect"
		5: return "channeling"
		_: return "nothing"

onready var val = {
	STATUS.none: [preload("res://assets/textures/user_interface/status/sleep.png")],
	STATUS.root: [preload("res://assets/textures/user_interface/status/paralize.png")],
	STATUS.silence: [preload("res://assets/textures/user_interface/status/silence.png")],
	STATUS.poison: [preload("res://assets/textures/user_interface/status/poison.png")],
	STATUS.protect: [preload("res://assets/textures/user_interface/status/protect.png")],
	STATUS.channeling: [preload("res://assets/textures/user_interface/status/sleep.png")] # TODO change
} 

func get_icon( status_id ):
	return val[status_id][0]