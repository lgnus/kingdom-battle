shader_type canvas_item;
render_mode unshaded;

// Gradient Colors
uniform vec4 main_color : hint_color;
uniform vec4 second_color : hint_color;

// Alpha mask to shape the 'liquid'
uniform sampler2D alpha_texture : hint_albedo;

// Surface size and color
uniform vec4 surface_color : hint_color;
uniform float surface_size : hint_range(0, 1);

// Variables to handle the frequency and amplitude of the waves
uniform float amplitude = 1;
uniform float frequency = 1;

//
uniform float fill_offset : hint_range(0,1);

// Convert RBG values to grayscale
float grayscale( vec4 color ) {
	return dot(color.rgb, vec3(0.299, 0.587, 0.114));
}

// random function 
float rand( vec2 uv , float time){
	return fract(sin(dot(uv * time, vec2(12.9898, 78.233))) * 43758.5453123);
}

void fragment() {
	
	
	
	highp float true_amp = amplitude * 0.01;
	
	// Main sin function
	highp float y = true_amp * (sin(SCREEN_PIXEL_SIZE.x * frequency) + 1.0) / 2.0;
	
	// Adjust these values around until satisfied
	highp float t = 0.01*(-TIME * 100.0);
	y += sin(SCREEN_PIXEL_SIZE.x * frequency * 4.35 + t) * 7.0;
	y += sin(SCREEN_PIXEL_SIZE.x * frequency * 1.65 + t * 0.49) * 4.0;
	y += sin(SCREEN_PIXEL_SIZE.x * frequency * 3.24 + t * 1.35) * 5.0;
	y += sin(SCREEN_PIXEL_SIZE.x * frequency * 2.66 + t * 3.67) * 2.5;
	y *= true_amp * 0.06;
	
	y += fill_offset;
	
	// If the value is below the main function, set alpha to 0
	if(SCREEN_PIXEL_SIZE.y < y ){
		COLOR.a = 0.0; 
	} else if (SCREEN_PIXEL_SIZE.y < y + surface_size){ // set surface color 
		COLOR = vec4(surface_color.rgb, 1.0); 
	} else { // set gradient color
		COLOR = mix(main_color, second_color, SCREEN_PIXEL_SIZE.y); 
	}
	
	// if an alpha mask is provided, white -> shown, black -> not
	if(grayscale(texture(alpha_texture, SCREEN_PIXEL_SIZE, 1).rgba) < 0.5){
		COLOR.a = 0.0;
	}
	
	// DEBUG:
	//COLOR = vec4(vec3(rand(UV, TIME)), 1.0);
}

