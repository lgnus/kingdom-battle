shader_type canvas_item;

void fragment() {
	vec4 pixel = texture(SCREEN_TEXTURE, SCREEN_UV);
	
	COLOR = pixel;
}